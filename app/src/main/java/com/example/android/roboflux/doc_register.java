package com.example.android.roboflux;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.provider.Settings;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.api.GoogleApiClient;
import com.google.android.gms.location.FusedLocationProviderApi;
import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.location.LocationServices;

public class doc_register extends AppCompatActivity implements GoogleApiClient.ConnectionCallbacks, GoogleApiClient.OnConnectionFailedListener, com.google.android.gms.location.LocationListener {

    EditText ET_NAME, ET_CONTACT, ET_USER_NAME, ET_SERVICE_PASS, ET_ADDRESS;
    String name, user_name, contact, password, address,latitude,longitude;
    //int contact;
    double lat,lon;

    private FusedLocationProviderApi locationProvider = LocationServices.FusedLocationApi;
    private GoogleApiClient googleApiClient;
    private LocationRequest locationRequest;
    public Double myLatitude;
    public Double myLongitude;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doc_register);
        ET_NAME = (EditText) findViewById(R.id.new_name);
        ET_USER_NAME = (EditText) findViewById(R.id.new_user_name);
        ET_SERVICE_PASS = (EditText) findViewById(R.id.new_pass);
        ET_ADDRESS = (EditText) findViewById(R.id.new_address);
        ET_CONTACT=(EditText)findViewById(R.id.new_contact);

        googleApiClient = new GoogleApiClient.Builder(this)
                .addApi(LocationServices.API)
                .addConnectionCallbacks(this)
                .addOnConnectionFailedListener(this)
                .build();

        locationRequest = new LocationRequest();
        locationRequest.setInterval(10 * 1000);
        locationRequest.setFastestInterval(15 * 1000);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    public void service_Register(View view) {
        name = ET_NAME.getText().toString();
        user_name = ET_USER_NAME.getText().toString();
        contact = ET_CONTACT.getText().toString();
        password = ET_SERVICE_PASS.getText().toString();
        address = ET_ADDRESS.getText().toString();
        latitude=Double.toString(myLatitude);
        longitude=Double.toString(myLongitude);

/*
        if(loc==null){
        lat=loc.sendLatitude();
        lon=loc.sendLongitude();

        latitude=Double.toString(loc.sendLatitude());
        longitude=Double.toString(loc.sendLongitude());
        }

        Toast.makeText(this,"lat: "+myLatitude+"lon: "+myLongitude,Toast.LENGTH_LONG).show();
*/        //contact=Integer.parseInt(ET_CONTACT.getText().toString());
        String method = "service_register";
        BackgroundTask backgroundTask = new BackgroundTask(this);
        backgroundTask.execute(method, name,user_name,contact,address,latitude,longitude,password);

        startActivity(new Intent(this, patient.class));

    }
    public void service_login(View view) {

        startActivity(new Intent(this, MainActivity.class));
    }



    public void requestLocationUpdates() {
        if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling
            //    ActivityCompat#requestPermissions
            // here to request the missing permissions, and then overriding
            //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
            //                                          int[] grantResults)
            // to handle the case where the user grants the permission. See the documentation
            // for ActivityCompat#requestPermissions for more details.
            return;
        }
        LocationServices.FusedLocationApi.requestLocationUpdates(googleApiClient, locationRequest, this);
    }

    @Override
    public void onConnected(@Nullable Bundle bundle) {
        requestLocationUpdates();
    }

    @Override
    public void onConnectionSuspended(int i) {

    }

    @Override
    public void onLocationChanged(Location location) {
        myLatitude = location.getLatitude();
        myLongitude = location.getLongitude();
        //latitudeText.setText("Latitude : " + String.valueOf(myLatitude));
        //longitudeText.setText("Longitude : " + String.valueOf(myLongitude));
        //sendLatitude();
        //sendLongitude();
        Toast.makeText(this,"lat: "+myLatitude+"lon: "+myLongitude,Toast.LENGTH_LONG).show();

    }

    public double getLatitude(){

        return myLatitude;
    }

    public double getLongitude(){
        return myLongitude;
    }


    @Override
    protected void onStart() {
        super.onStart();
        googleApiClient.connect();
    }

    @Override
    protected void onResume() {
        super.onResume();
        if (googleApiClient.isConnected()) {
            requestLocationUpdates();
        }
    }

    @Override
    protected void onPause() {
        super.onPause();
        LocationServices.FusedLocationApi.removeLocationUpdates(googleApiClient, this);
    }

    @Override
    protected void onStop() {
        super.onStop();
        googleApiClient.disconnect();
    }




    @Override
    public void onConnectionFailed(@NonNull ConnectionResult connectionResult) {

    }
}



